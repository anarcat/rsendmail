# 1.1.4 / 2023-02-02

 * more Debian packaging fixes

# 1.1.2 / 2023-01-26

 * Debian packaging and documentation fixes

1.1.1 / 2020-07-08
==================

  * ship the right readme file

1.1.0 / 2020-07-08
==================

  * move metadata out of modules, improve test suite, and CI
  * fix install through pip
  * fix ssh key deployment instruction
  * improve on error messages
  * make rsendmail work with normal PATH
  * add a POC: explain buffering gymnastics

1.0.1 / 2018-05-07
==================

Minor bugfix release after thorough testing on nullmailer.

  * handle unicode errors from nullmailer
  * properly decode envelope from nullmailer2
  * fail early when detecting nullmailer 1.x

1.0.0 / 2018-04-24
==================

  * first stable release
