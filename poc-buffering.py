#!/usr/bin/python3

import subprocess

# this program should be showing the first paragraph prefixed with
# `first:` and the remaining lines prefixed with `remaining:`.
#
# the point of this program is to show that this would *not* work with
# a normal `open` call, which has `buffering=1` and would therefore
# slurp more than what we are displaying in the loop.
stream = open('/etc/hosts', mode='rb', buffering=0)

for line in stream:
    print("first: %s" % line.decode('utf8'), end='')
    if line == b"\n":
        break

proc = subprocess.Popen(['sed', 's/^/remaining:/'], stdin=stream, close_fds=False)
proc.wait()
